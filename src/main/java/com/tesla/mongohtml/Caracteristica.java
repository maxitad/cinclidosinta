/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tesla.mongohtml;

import com.tesla.mongohtml.basededatos.DBConnector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tadeo
 */
public class Caracteristica {
    private String idFTipoExamen;
    private String  nombre;
    private String idCaracteristica;

    public Caracteristica(){
        
    }
    public String getIdCaracteristica() {
        return idCaracteristica;
    }

    public Caracteristica setIdCaracteristica(String idCaracteristica) {
        this.idCaracteristica = idCaracteristica;
        return this;
    }

    public String getIdFTipoExamen() {
        return idFTipoExamen;
    }

    /**
     * Determina qué tipo de examen es, Visual 1, Olfativo 2, Gustativo 3. 
     * @param idTipoExamen
     * @return 
     */
    public Caracteristica setIdFTipoExamen(String idTipoExamen) {
        this.idFTipoExamen = idTipoExamen;
        return this;
    }

    public String getNombre() {
        return nombre;
    }

    public Caracteristica setNombre(String nombre) {
        this.nombre = nombre;
        return this;
    }
    
    public Integer guardarExamen(){
        DBConnector dbc;
        boolean rs = false;
        dbc = new DBConnector();
        try{
            rs = dbc.nuevaCaracteristica(this);
        } catch(Exception ex){
            Logger.getLogger(Caracteristica.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(rs){
            return 1;
        }else{
            System.out.println("Error al guardar una nueva Caracteristica.");
            return -1;
        }
    }
}
