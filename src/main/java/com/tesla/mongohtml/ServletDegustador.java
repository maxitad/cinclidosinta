/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tesla.mongohtml;

import com.google.gson.Gson;
import com.tesla.mongohtml.basededatos.DBConnector;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tadeo
 */
public class ServletDegustador extends HttpServlet {

    private static final int CARGATRATAMIENTOS = 0;
    ArrayList<Tratamiento> tratamientos = null;

     protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        DBConnector dbc = new DBConnector();
        try {

            Integer operacion;
            Object prueba;
            try {
                prueba = request.getParameter("op");
                operacion = Integer.valueOf(request.getParameter("op"));
            } catch (NumberFormatException e) {
                operacion = -1;
            }

            switch (operacion) {
                
                /**
                 * Carga los vinos guardados en la BD.
                 * Relacionando el ID de la sesión activa con la tabla template_tratamientos.
                 */
                case CARGATRATAMIENTOS:
                    System.out.println("Entre en CARGO TRATAMIENTO");
                    tratamientos = dbc.tratamientosSesion();
                    out.print((new Gson().toJson(tratamientos)));
                    System.out.println((new Gson().toJson(tratamientos)));
                    break;
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ServletDegustador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ServletDegustador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
