package com.tesla.mongohtml.basededatos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataBase {

    private String bd = "mongointa"; //definimos la base de datos
    private String user = "root"; //definimos el user
    private String password = "Sorrentin0s"; //definimos pass
    private String url = "jdbc:mysql://localhost/" + bd + "?useServerPrepStmts=true"; //definimos la dirección de la DB
    private String driver = "com.mysql.jdbc.Driver"; //definimos el Driver
    private Connection conexion = null;

    public Connection establececonexion() {
        try {
            //establecemos la conexión con la base de datos
            Class.forName(driver).newInstance();
            conexion = (Connection) DriverManager.getConnection(url, user, password);
        } catch (Exception e) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, "Error al Conectarse a la Base de Datos", e);
        }
        return conexion;
    }

    public void cierraConexion() {
        //se cierra la conexión con la base de datos
        try {
            conexion.close();
        } catch (Exception e) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, "Error al Cerrar la Base de Datos", e);
        }
    }

    public ResultSet realizarConsulta(String consulta) {
        ResultSet rs = null;
        System.out.println(consulta);
        try {
            if (conexion != null) {
                PreparedStatement spt = (PreparedStatement) conexion.prepareStatement(consulta);
                rs = (ResultSet) spt.executeQuery();
            }
            return rs;
        } catch (Exception e) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, "Error al ejecutar la consulta", e);
        }
        return rs;
    }

    public Boolean actualizar(String update) {
        int retorno = -1;
        try {
            if (conexion != null) {
                PreparedStatement spt = (PreparedStatement) conexion.prepareStatement(update);
                retorno = spt.executeUpdate();
            }
        } catch (Exception e) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, "Error al ejecutar la actualización del registro", e);
        }
        return retorno > 0;
    }
}
