/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tesla.mongohtml.basededatos;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import com.tesla.mongohtml.Caracteristica;
import com.tesla.mongohtml.Degustador;
import com.tesla.mongohtml.SesionDegustacion;
import com.tesla.mongohtml.Tratamiento;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author tadeo
 */
public class DBConnector {

    private Connection conector = null;
    private Statement statement = null;
    private ResultSet resultset = null;

    /**
     * Lógica de logeo para comprobar a un administrador. Tener en cuenta que el <em>ResultSet</em> tiene complicaciones
     * al momento de retornar un valor.
     *
     * @param user
     * @param pass
     * @return
     * @throws Exception
     */
    public boolean inLogin(String user, String pass) throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conector = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/cinclidos_inta", "root", "Sorrentin0s");
            statement = (Statement) conector.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            resultset = statement.executeQuery("select * from administradores where usuario=" + "\"" + user + "\"" + " and clave=" + "\"" + pass + "\"");
            String pcv1 = null;
            String pcv2 = null;
            while (resultset.next()) {
                pcv1 = resultset.getString("usuario");
                pcv2 = resultset.getString("clave");
            }
            conector.close();
            resultset.close();
            if (pcv1.equals(user) && pcv2.equals(pass)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (resultset != null) {
                try {
                    resultset.close();
                } catch (SQLException e) {
                    System.out.println(e);
                }
            }

        }

    }

    /**
     * Método para guardar un nuevo Tratamiento
     *
     * @param varietal
     * @return
     * @throws Exception
     */
    public boolean nuevoVarietal(Tratamiento varietal) throws Exception {
        boolean rest = false;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conector = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/cinclidos_inta", "root", "Sorrentin0s");
            statement = (Statement) conector.createStatement();
            String sql = "insert into `tratamientos`(`nombreEnsayo`, `nombreOrigen`, `anioProduccion`) values "
                    + "(" + "'" + varietal.getNombre() + "'" + ","
                    + "'" + varietal.getOrigen() + "',"
                    + "'" + varietal.getAñoCosecha() + "'" + ")";
            System.out.println(sql);
            int executeUpdate = statement.executeUpdate(sql);
            rest = true;
            statement.close();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            conector.close();

        }
        return rest;
    }

    /**
     * Método que consulta a la BD por las características del tipo Gustativas. Crea un objeto Característica por cada
     * uno hallado y lo inserta en una lista.
     *
     * @return ArrayList<Caracteristica>
     * @throws Exception
     */
    public ArrayList<Caracteristica> examenVisual() throws Exception {
        ArrayList<Caracteristica> listacar = new ArrayList<Caracteristica>();

        try {
            Class.forName("com.mysql.jdbc.Driver");
            conector = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/cinclidos_inta", "root", "Sorrentin0s");
            statement = (Statement) conector.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String sql = "SELECT * FROM `examenes` WHERE `idFTipoExamen` = 1";
            System.out.println(sql);
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                Caracteristica caramba = new Caracteristica();
                System.out.println("Tamaño de la tabla: " + rs);
                caramba.setIdCaracteristica(rs.getString(1));
                caramba.setIdFTipoExamen(rs.getString(2));
                caramba.setNombre(rs.getString(3));
                listacar.add(caramba);
            }

            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            conector.close();
        }
        return listacar;
    }

    /**
     * Método que consulta a la BD por las caracerísticas Visual.
     *
     * @return
     * @throws Exception
     */
    public ArrayList<Caracteristica> examenOlfativo() throws Exception {
        ArrayList<Caracteristica> listacar = new ArrayList<Caracteristica>();
        System.out.println("Consulta de Examen Visual");
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conector = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/cinclidos_inta", "root", "Sorrentin0s");
            statement = (Statement) conector.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String sql = "SELECT * FROM `examenes` WHERE `idFTipoExamen` = 2";
            System.out.println(sql);
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                Caracteristica caramba = new Caracteristica();
                System.out.println("Tamaño de la tabla: " + rs);
                caramba.setIdCaracteristica(rs.getString(1));
                caramba.setIdFTipoExamen(rs.getString(2));
                caramba.setNombre(rs.getString(3));
                listacar.add(caramba);
            }

            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            conector.close();
        }
        return listacar;
    }

    /**
     * Método que consulta a la BD por las caracerísticas Visual.
     *
     * @return
     * @throws Exception
     */
    public ArrayList<Caracteristica> examenGustativo() throws Exception {
        ArrayList<Caracteristica> listacar = new ArrayList<Caracteristica>();
        System.out.println("Consulta de Examen Olfativo");
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conector = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/cinclidos_inta", "root", "Sorrentin0s");
            statement = (Statement) conector.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String sql = "SELECT * FROM `examenes` WHERE `idFTipoExamen` = 3";
            System.out.println(sql);
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                Caracteristica caramba = new Caracteristica();
                System.out.println("Tamaño de la tabla: " + rs);
                caramba.setIdCaracteristica(rs.getString(1));
                caramba.setIdFTipoExamen(rs.getString(2));
                caramba.setNombre(rs.getString(3));
                listacar.add(caramba);
            }

            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            conector.close();
        }
        return listacar;
    }

    /**
     * Método para obtener todos los <i>vinos</i> guardados en la BD.
     *
     * @return
     * @throws Exception
     */
    public ArrayList<Tratamiento> tratamientos() throws Exception {
        ArrayList<Tratamiento> listatrat = new ArrayList<Tratamiento>();
        System.out.println("Consulta por TRATAMIENTOS");
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conector = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/cinclidos_inta", "root", "Sorrentin0s");
            statement = (Statement) conector.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String sql = "SELECT * FROM `tratamientos`";
            System.out.println(sql);
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                Tratamiento tratos = new Tratamiento();
                System.out.println("La tabla es: " + rs);
                tratos.setId(rs.getString(1));
                tratos.setNombre(rs.getString(2));
                tratos.setOrigen(rs.getString(3));
                tratos.setAñoCosecha(rs.getString(4));
                listatrat.add(tratos);
            }

            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            conector.close();
        }
        return listatrat;
    }

    /**
     * Método para registrar un nuevo usuario.
     *
     * @param degustador
     * @return
     * @throws SQLException
     */
    public Integer nuevoDegustador(Degustador degustador) throws SQLException {
        Integer rest = null;
        try {
            Integer idSesion = existeSesion();
            Class.forName("com.mysql.jdbc.Driver");
            conector = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/cinclidos_inta", "root", "Sorrentin0s");
            statement = (Statement) conector.createStatement();
            String sql = "insert into `degustadores`(`nombre`, `apellido`, `procedencia`,`idTemplate` ) values "
                    + "(" + "'" + degustador.getNombre() + "'" + ","
                    + "'" + degustador.getApellido() + "',"
                    + "'" + degustador.getProcedencia() + "',"
                    + "'" + idSesion + "')";
            System.out.println(sql);
            rest = statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            statement.close();
        } catch (Exception e) {
            System.out.println(e);
            rest = -1; //cuando existe un error en la consulta
        } finally {
            conector.close();
        }
        return rest;
    }

    /**
     * Método para activar una sesión.
     *
     * @return
     * @throws SQLException
     */
    public Integer sesionActiva() throws SQLException {
        Integer rest = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conector = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/cinclidos_inta", "root", "Sorrentin0s");
            statement = (Statement) conector.createStatement();
            String sql = "SELECT * FROM `templates` ORDER BY idTemplate DESC LIMIT 1;";
            System.out.println(sql);
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                rest = rs.getInt(1);
            }
            statement.close();

        } catch (Exception e) {
            System.out.println(e);
            rest = -1; //cuando existe un error en la consulta
        } finally {
            conector.close();
        }
        return rest;
    }

    /**
     * Método que permite guardar una nueva característica o examen.
     *
     * @param caracteristica
     * @return
     * @throws Exception
     */
    public boolean nuevaCaracteristica(Caracteristica caracteristica) throws Exception {
        boolean rest = false;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conector = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/cinclidos_inta", "root", "Sorrentin0s");
            statement = (Statement) conector.createStatement();
            String sql = "insert into `examenes`(`idFTipoExamen`, `nombre`) values "
                    + "(" + "'" + caracteristica.getIdFTipoExamen() + "'" + ","
                    + "'" + caracteristica.getNombre() + "'" + ")";
            System.out.println(sql);
            int executeUpdate = statement.executeUpdate(sql);
            rest = true;
            statement.close();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            conector.close();

        }
        return rest;
    }

    /**
     * Método que guarda todos los elementos necesarios para crear una sesión de degustación.
     *
     * @param sd
     * @return boolean verdadero en caso de que todo haya sido guardado correctamente.
     * @throws Exception
     */
    public boolean nuevaSesionCompleta(SesionDegustacion sd) throws Exception {
        boolean rest = false;
        String[] tratamientos = sd.getTratamientosSeleccionados();
        String[] examenes = sd.getExamenesSeleccionados();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conector = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/cinclidos_inta", "root", "Sorrentin0s");
            statement = (Statement) conector.createStatement();

            String sql = "insert into templates(nombre, activo) values" + "(" + "'" + sd.getNombre() + "'," + 0 + ")";
            System.out.println(sql);
            int executeUpdate = statement.executeUpdate(sql);
            String idTemplate = "SELECT idTemplate FROM templates ORDER BY idTemplate DESC LIMIT 1";
            ResultSet rs = statement.executeQuery(idTemplate);
            Integer ultimoIdInsertado = 0;
            while (rs.next()) {
                ultimoIdInsertado = rs.getInt(1);
                System.out.println("ID del último ingresado " + ultimoIdInsertado);
            }

            for (int i = 0; i < tratamientos.length; i++) {
                System.out.println("Tratamientos seleccionados para guardar en esta sesión: " + tratamientos[i]);
                sql = "insert into template_tratamientos(idFTemplate, idFTratamiento) values" + "(" + ultimoIdInsertado + "," + tratamientos[i] + ")";
                int exectinsert = statement.executeUpdate(sql);
            }
            for (int i = 0; i < examenes.length; i++) {
                System.out.println("Examenes seleccionados para guardar en esta sesión: " + examenes[i]);
                sql = "insert into template_examenes(idFTemplate, idFExamen) values" + "(" + ultimoIdInsertado + "," + examenes[i] + ")";
                int exectinsert = statement.executeUpdate(sql);
            }
            rest = true;
        } catch (Exception e) {
            System.out.println(e);
        }
        return rest;
    }

    /**
     * Método que consulta por todas las sesiones creadas.
     *
     * @return ArrayList de tipo SesionDegustacion.
     * @throws Exception
     */
    public ArrayList<SesionDegustacion> cargarSesiones() throws Exception {
        ArrayList<SesionDegustacion> sesiones = new ArrayList<SesionDegustacion>();
        System.out.println("Consulta por las SESIONES CREADAS");
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conector = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/cinclidos_inta", "root", "Sorrentin0s");
            statement = (Statement) conector.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String sql = "SELECT * FROM `templates`";
            System.out.println(sql);
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                SesionDegustacion template = new SesionDegustacion();
                System.out.println("La tabla es: " + rs);
                template.setIdTemplate(rs.getString(1));
                template.setNombre(rs.getString(2));
                template.setActivo(rs.getInt(3));
                sesiones.add(template);
            }

            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            conector.close();
        }
        return sesiones;
    }

    /**
     * Consulta por la existencia de alguna sesión activa para poder cargar datos.
     *
     * @return
     */
    public Integer existeSesion() {
        System.out.println("Consulta por las SESIONES CREADAS");
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conector = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/cinclidos_inta", "root", "Sorrentin0s");
            statement = (Statement) conector.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String sql = "select * from templates where activo = 1;";
            System.out.println(sql);
            ResultSet rs = statement.executeQuery(sql);
            if (rs.next()) {
                return rs.getInt(1);
            } else {
                return 0;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return 0;
    }

    public boolean activarSesion(String idTemplate) {
        boolean rlz = false;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conector = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/cinclidos_inta", "root", "Sorrentin0s");
            statement = (Statement) conector.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String sql = "update templates set activo=1 where idTemplate=" + idTemplate + ";";
            int rs = statement.executeUpdate(sql);
            System.out.println(sql + rs);
            rlz = true;
        } catch (Exception e) {
            System.out.println(e);
        }
        return rlz;
    }

    public ArrayList<Tratamiento> tratamientosSesion() throws Exception {
        ArrayList<Tratamiento> listatrat = new ArrayList<Tratamiento>();
        System.out.println("Consulta por TRATAMIENTOS");
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conector = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/cinclidos_inta", "root", "Sorrentin0s");
            statement = (Statement) conector.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String sql = "select idTratamiento, nombreEnsayo, nombreOrigen, anioProduccion from tratamientos as tr left join template_tratamientos as tt "
                    + "on tr.idTratamiento= tt.idFTratamiento left join templates as tm on tm.idTemplate = tt.idFTemplate where tm.activo = 1;;";
            System.out.println(sql);
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                Tratamiento tratos = new Tratamiento();
                System.out.println("La tabla es: " + rs);
                tratos.setId(rs.getString(1));
                tratos.setNombre(rs.getString(2));
                tratos.setOrigen(rs.getString(3));
                tratos.setAñoCosecha(rs.getString(4));
                listatrat.add(tratos);
            }

            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            conector.close();
        }
        return listatrat;
    }
}