/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tesla.mongohtml;

import com.tesla.mongohtml.basededatos.DBConnector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tadeo
 */
public class Degustador {
    private String idDegustador;
    private String nombre;
    private String apellido;
    private String procedencia;

    public String getApellido() {
        return this.apellido;
    }

    public Degustador setApellido(String apellido) {
        this.apellido = apellido;
        return this;
    }

    public String getIdDegustador() {
        return this.idDegustador;
    }

    public void setIdDegustador(String idDegustador) {
        this.idDegustador = idDegustador;
    }

    public String getNombre() {
        return this.nombre;
    }

    public Degustador setNombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public String getProcedencia() {
        return this.procedencia;
    }

    public Degustador setProcedencia(String procedencia) {
        this.procedencia = procedencia;
        return this;
    }
    /**
     * Método para guardar los datos de un nuevo degustador.
     * Devuelva indice de usuario en la tabla o -1 si es incorrecto o no existe.
     * @return 
     */
    public Integer guardarDesgustador() {
        //
        DBConnector dbc;
        Integer rs = null;
        dbc = new DBConnector();
        try {
             rs = dbc.nuevoDegustador(this);

        } catch (Exception ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (rs != null) {
            return rs;
        } else {
            System.out.println("Error al ingresar. Usuario o contraseña inválidos.");
            return null;
        }
    }
}
