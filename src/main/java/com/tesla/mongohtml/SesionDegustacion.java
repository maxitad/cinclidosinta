/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tesla.mongohtml;

import com.tesla.mongohtml.basededatos.DBConnector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tadeo
 */
public class SesionDegustacion {
    private String idTemplate;
    private String nombre;
    private int activo;
    private String[] tratamientosSeleccionados;

    public String[] getTratamientosSeleccionados() {
        return tratamientosSeleccionados;
    }

    public SesionDegustacion setTratamientosSeleccionados(String[] tratamientosSeleccionados) {
        this.tratamientosSeleccionados = tratamientosSeleccionados;
        return this;
    }

    public String[] getExamenesSeleccionados() {
        return ExamenesSeleccionados;
    }

    public SesionDegustacion setExamenesSeleccionados(String[] ExamenesSeleccionados) {
        this.ExamenesSeleccionados = ExamenesSeleccionados;
        return this;
    }
    private String[] ExamenesSeleccionados;
    
    public String getIdTemplate() {
        return idTemplate;
    }

    public SesionDegustacion setIdTemplate(String idTemplate) {
        this.idTemplate = idTemplate;
        return this;
    }

    public String getNombre() {
        return nombre;
    }

    public SesionDegustacion setNombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public int getActivo() {
        return activo;
    }

    public SesionDegustacion setActivo(int activo) {
        this.activo = activo;
        return this;
    }
    /**
     * Guarda una sesión con todos los datos relacionados con los examenes y los tratamientos
     * @param tratamientosSeleccionados
     * @param ExamenesSeleccionados
     * @return 
     */
    public Integer guardarSesion(){
        DBConnector dbc;
        boolean rs = false;
        dbc = new DBConnector();
        try{
            rs = dbc.nuevaSesionCompleta(this);
        } catch(Exception ex){
            Logger.getLogger(Caracteristica.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(rs){
            return 1;
        }else{
            System.out.println("Error al guardar una nueva Caracteristica.");
            return -1;
        }
    }
}