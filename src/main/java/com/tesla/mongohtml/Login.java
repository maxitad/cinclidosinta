package com.tesla.mongohtml;

import com.tesla.mongohtml.basededatos.DBConnector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Login {

    private String usuario;
    private String contraseña;
/**
 * Clase para el Login del administrador.
 */
    public Login() {
    }

    public String getContraseña() {
        return contraseña;
    }

    public Login setContraseña(String contraseña) {
        this.contraseña = contraseña;
        return this;
    }

    public String getUsuario() {
        return usuario;
    }

    public Login setUsuario(String usuario) {
        this.usuario = usuario;
        return this;
    }

    public Integer autenticar() {
        System.out.println("###AUTENTICAR ADMINISTRADOR###");
        //logica de logeo devuelva indice de usuario en la tabla o -1 si es incorrecto o no existe
        DBConnector dbc;
        boolean rs = false;
        dbc = new DBConnector();
        try {
             rs = dbc.inLogin(this.usuario, this.contraseña);

        } catch (Exception ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (rs) {
            return 1;
        } else {
            System.out.println("Error al ingresar. Usuario o contraseña inválidos.");
            return -1;
        }
    }
}
