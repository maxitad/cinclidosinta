/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tesla.mongohtml;

import com.tesla.mongohtml.basededatos.DBConnector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tadeo
 */
public class Tratamiento {
    private String nombre;
    private String origen;
    private String añoCosecha;
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public Tratamiento(){
        
    }

    public String getAñoCosecha() {
        return añoCosecha;
    }

    public Tratamiento setAñoCosecha(String añoCosecha) {
        this.añoCosecha = añoCosecha;
        System.out.println("###"+añoCosecha);
        
        return this;
    }

    public String getOrigen() {
        return origen;
    }

    public Tratamiento setOrigen(String bodega) {
        this.origen = bodega;
        System.out.println("###"+bodega);
        return this;
    }

    public String getNombre() {
        return nombre;
    }

    public Tratamiento setNombre(String nombre) {
        this.nombre = nombre;
        System.out.println("####"+nombre);
        return this;
    }
    
    public Integer guardarVarietal(){
        DBConnector dbc;
        boolean rs = false;
        dbc = new DBConnector();
        try{
            rs = dbc.nuevoVarietal(this);
        } catch(Exception ex){
            Logger.getLogger(Tratamiento.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(rs){
            return 1;
        }else{
            System.out.println("Error al guardar un nuevo Varietal.");
            return -1;
        }
    }
    
    
}
