/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tesla.mongohtml;

import com.google.gson.Gson;
import com.tesla.mongohtml.basededatos.DBConnector;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author heberlz
 */
public class ServletMaestro extends HttpServlet {

    private static final int REGISTROCATADOR = 0;
    private static final int LOGIN = 1;
    private static final int NVARIETAL = 2;
    private static final int CARGAGUSTATIVO = 3;
    private static final int CARGAVISUAL = 4;
    private static final int CARGOLFATIVO = 5;
    private static final int CARGOTRATAMIENTOS = 6;
    private static final int DATOSSESIONNUEVA = 7;
    private static final int ENVIARSESION = 8;
    private static final int CONSULTARSESION = 9;
    private static final int CERRARSESIONADMIN = 10;
    private static final int NUEVACARACTERISTICA = 11;
    private static final int MOSTRARSESIONES = 12;
    private static final int LOGOUT = 13;
    private static final int ACTIVARSESION = 14;
    private static final int EXISTESESION = 15;
    private ArrayList<Caracteristica> caracteristicas = null;
    private ArrayList<Tratamiento> tratamientos = null;
    private ArrayList<SesionDegustacion> sesiones = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        DBConnector dbc = new DBConnector();
        try {

            Integer operacion;
            Object prueba;
            try {
                prueba = request.getParameter("op");
                operacion = Integer.valueOf(request.getParameter("op"));
            } catch (NumberFormatException e) {
                operacion = -1;
            }

            switch (operacion) {
                case REGISTROCATADOR:
                    System.out.println("Un nuevo DEGUSTADOR!");
                    /**
                     * Inicia sesión una persona para entrar en la catación.
                     */
                    Integer degus;
                    if (-1 != (degus = new Degustador().
                            setNombre((String) request.getParameter("nombre")).
                            setApellido((String) request.getParameter("apellido")).
                            setProcedencia((String) request.getParameter("procedencia")).
                            guardarDesgustador())) {
                        request.getSession(true).setAttribute("degus", degus);
                        out.print("{\"status\":\"ok\"}");
                    } else {
                        out.print("{\"status\":\"notOk\"}");
                    }
                    break;
                case LOGIN:
                    System.out.println("entrando en login");
                    /**
                     * Session ID (id de el usuario en la tabla) para el admin.
                     */
                    Integer sid;
                    if (-1 != (sid = new Login().setUsuario((String) request.getParameter("username")).
                            setContraseña((String) request.getParameter("password")).autenticar())) {
                        System.out.println("username: " + request.getParameter("username") + "pass: " + request.getParameter("password"));
                        request.getSession(true).setAttribute("sid", sid);
                        out.print("{\"status\":\"ok\"}");
                    } else {
                        if (null != request.getSession(true).getAttribute("sid")) {
                            request.getSession(true).removeAttribute("sid");
                        }
                        out.print("{\"status\":\"notOk\"}");
                    }
                    break;
                case LOGOUT:
                    System.out.println("Salida del modo administrador.f");
                    request.getSession(true).removeAttribute("sid");
                    break;
                /**
                 * Guarda un nuevo varietal.
                 */
                case NVARIETAL:
                    Integer vari;
                    if (-1 != (vari = new Tratamiento().setNombre((String) request.getParameter("nombreVarietal")).setOrigen((String) request.getParameter("bodegaNombre")).setAñoCosecha((String) request.getParameter("anioCosecha")).guardarVarietal())) {
                        System.out.println("VARI " + vari);
                        out.print("{\"status\":\"ok\"}");
                    } else {
                        out.print("{\"status\":\"notOk\"}");
                    }
                    break;
                /**
                 * Carga las listas de opciones.
                 */
                case CARGAGUSTATIVO:
                    System.out.println("Entre en CARGA GUSTATIVO");
                    caracteristicas = dbc.examenGustativo();
                    String datos = new Gson().toJson(caracteristicas);
                    out.print(datos);
                    System.out.println(datos);
                    break;

                case CARGAVISUAL:
                    System.out.println("Entre en CARGA VISUAL");
                    caracteristicas = dbc.examenVisual();
                    out.print((new Gson().toJson(caracteristicas)));
                    System.out.println(new Gson().toJson(caracteristicas));
                    break;

                case CARGOLFATIVO:
                    System.out.println("Entre en CARGA OLFATIVO");
                    caracteristicas = dbc.examenOlfativo();
                    out.print((new Gson().toJson(caracteristicas)));
                    System.out.println(new Gson().toJson(caracteristicas));
                    break;
                /**
                 * Carga los vinos guardados en la BD.
                 */
                case CARGOTRATAMIENTOS:
                    System.out.println("Entre en CARGO TRATAMIENTO");
                    tratamientos = dbc.tratamientos();
                    out.print((new Gson().toJson(tratamientos)));
                    System.out.println((new Gson().toJson(tratamientos)));
                    break;
                /**
                 * Guarda en la base de datos los elementos de una nueva sesión.
                 */
                case DATOSSESIONNUEVA:
                    Integer sesion;
                    System.out.println("Entre en DATOS SESION NUEVA");
                    String[] tratamientosSeleccionados = request.getParameter("tratamientos").toString().split(" ");
                    String[] ExamenesSeleccionados = request.getParameter("examenes").toString().split(" ");
                    String nombre = request.getParameter("nombre").toString();
                    System.out.println("nombre: " + nombre);

                    nombre = cambiarLetras(nombre);

                    System.out.println("nombre de nuevo: " + nombre);

                    for (int i = 0; i < tratamientosSeleccionados.length; i++) {
                        System.out.println("Tratamientos seleccionados " + tratamientosSeleccionados[i]);
                    }

                    for (int i = 0; i < ExamenesSeleccionados.length; i++) {
                        System.out.println("Examenes seleccionados: " + ExamenesSeleccionados[i]);
                    }
                    if (-1 != (sesion = new SesionDegustacion().setActivo(1).setNombre(nombre).
                            setExamenesSeleccionados(ExamenesSeleccionados).
                            setTratamientosSeleccionados(tratamientosSeleccionados).guardarSesion())) {
                        System.out.println("Guardada la SESION: " + nombre + "!");
                    }
                    break;
                case CONSULTARSESION:
                    System.out.println("¿EXISTE UNA NUEVA SESIÓN?");
                    Integer existeId;
                    existeId = dbc.existeSesion();
                    System.out.println("La sesión activa tiene como ID: " + existeId);
                    out.print("{\"idSesion\":\""+existeId+"\"}");
                    break;
                case NUEVACARACTERISTICA:
                    Integer exam;
                    nombre = request.getParameter("nombre");
                    nombre = cambiarLetras(nombre);
                    if (-1 != (exam = new Caracteristica().setNombre((String) nombre).setIdFTipoExamen((String) request.getParameter("tipoEx")).guardarExamen())) {
                        System.out.println("CARACT " + exam);
                        out.print("{\"status\":\"ok\"}");
                    } else {
                        out.print("{\"status\":\"notOk\"}");
                    }
                    break;
                /**
                 * Devuelve todo lo que necesita admin.jsp para mostrar las sesiones creadas.
                 */
                case MOSTRARSESIONES:
                    System.out.println("Entre en MOSTRAR SESIONES");
                    sesiones = dbc.cargarSesiones();
                    out.print((new Gson().toJson(sesiones)));
                    System.out.println((new Gson().toJson(sesiones)));
                    break;
                case ACTIVARSESION:
                    System.out.println("Entre en ACTIVAR SESION");
                    String idSesion = request.getParameter("idSesion");
                    System.out.println("ID de la sesión: " + idSesion);
                    if (dbc.activarSesion(idSesion)) {
                        System.out.println("Resultó bien!");
                        out.print("{\"status\":\"ok\"}");
                    } else {
                        System.out.println("algo salió mal!");
                        out.print("{\"status\":\"notOk\"}");
                    }
                    break;
                case EXISTESESION:  
                    System.out.println("Entre en EXISTE SESIÓN");
                    Integer sesionActiva = dbc.sesionActiva();
                    if(sesionActiva!=null){
                        out.print("{\"idSesion\":\""+sesionActiva+"\"}");
                    }else{
                        out.print("{\"idSesion\":\"nula\"}");
                    }
                    break;
            }
        } finally {
            out.close();
        }
    }

    private String cambiarLetras(String variable) {

        variable = variable.replace("Ã¡", "á");
        variable = variable.replace("Ã©", "é");
        variable = variable.replace("Ã*", "í");
        variable = variable.replace("Ã³", "ó");
        variable = variable.replace("Ãº", "ú");
        variable = variable.replace("Ã", "Á");
        variable = variable.replace("Ã‰", "É");
        variable = variable.replace("Ã", "Í");
        variable = variable.replace("Ã“", "Ó");
        variable = variable.replace("Ãš", "Ú");
        variable = variable.replace("Ã±", "ñ");
        variable = variable.replace("Ã‘", "Ñ");
        return variable;
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ServletMaestro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ServletMaestro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
