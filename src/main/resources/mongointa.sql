-- MySQL dump 10.13  Distrib 5.5.24, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: mongointa
-- ------------------------------------------------------
-- Server version	5.5.24-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administradores`
--

DROP TABLE IF EXISTS `administradores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administradores` (
  `idAdministrador` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idAdministrador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administradores`
--

LOCK TABLES `administradores` WRITE;
/*!40000 ALTER TABLE `administradores` DISABLE KEYS */;
/*!40000 ALTER TABLE `administradores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `caracteristicas`
--

DROP TABLE IF EXISTS `caracteristicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caracteristicas` (
  `idCaracteristica` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `idTipoExamen` bigint(20) NOT NULL,
  PRIMARY KEY (`idCaracteristica`),
  KEY `idTipoExamen` (`idTipoExamen`),
  CONSTRAINT `caracteristicas_ibfk_1` FOREIGN KEY (`idTipoExamen`) REFERENCES `tiposexamenes` (`idTipoExamen`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caracteristicas`
--

LOCK TABLES `caracteristicas` WRITE;
/*!40000 ALTER TABLE `caracteristicas` DISABLE KEYS */;
INSERT INTO `caracteristicas` VALUES (1,'Intensidad',1);
INSERT INTO `caracteristicas` VALUES (2,'Matiz',1);
INSERT INTO `caracteristicas` VALUES (3,'Intensidad Global',2);
INSERT INTO `caracteristicas` VALUES (4,'Nota Vegetal',2);
INSERT INTO `caracteristicas` VALUES (5,'Nota Floral',2);
INSERT INTO `caracteristicas` VALUES (6,'Nota Frutal',2);
INSERT INTO `caracteristicas` VALUES (7,'Nota Especiada',2);
INSERT INTO `caracteristicas` VALUES (8,'Nota Balsámica',2);
INSERT INTO `caracteristicas` VALUES (9,'Nota Empyreumatico',2);
INSERT INTO `caracteristicas` VALUES (10,'Nota Animal',2);
INSERT INTO `caracteristicas` VALUES (11,'Nota Mineral',2);
INSERT INTO `caracteristicas` VALUES (12,'Alcohol',3);
INSERT INTO `caracteristicas` VALUES (13,'Acidez',3);
INSERT INTO `caracteristicas` VALUES (14,'Concentración',3);
INSERT INTO `caracteristicas` VALUES (15,'Untuosidad',3);
INSERT INTO `caracteristicas` VALUES (16,'Astringencia',3);
INSERT INTO `caracteristicas` VALUES (17,'Amargor',3);
INSERT INTO `caracteristicas` VALUES (18,'Persistencia',3);
/*!40000 ALTER TABLE `caracteristicas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `caracteristicas_templates`
--

DROP TABLE IF EXISTS `caracteristicas_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caracteristicas_templates` (
  `idTemplate` bigint(20) NOT NULL,
  `idCaracteristica` bigint(20) NOT NULL,
  PRIMARY KEY (`idTemplate`,`idCaracteristica`),
  KEY `idCaracteristica` (`idCaracteristica`),
  CONSTRAINT `caracteristicas_templates_ibfk_1` FOREIGN KEY (`idTemplate`) REFERENCES `templates` (`idTemplate`),
  CONSTRAINT `caracteristicas_templates_ibfk_2` FOREIGN KEY (`idCaracteristica`) REFERENCES `caracteristicas` (`idCaracteristica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caracteristicas_templates`
--

LOCK TABLES `caracteristicas_templates` WRITE;
/*!40000 ALTER TABLE `caracteristicas_templates` DISABLE KEYS */;
INSERT INTO `caracteristicas_templates` VALUES (1,1);
INSERT INTO `caracteristicas_templates` VALUES (1,2);
INSERT INTO `caracteristicas_templates` VALUES (1,3);
INSERT INTO `caracteristicas_templates` VALUES (1,4);
INSERT INTO `caracteristicas_templates` VALUES (1,5);
INSERT INTO `caracteristicas_templates` VALUES (1,6);
INSERT INTO `caracteristicas_templates` VALUES (1,7);
INSERT INTO `caracteristicas_templates` VALUES (1,8);
INSERT INTO `caracteristicas_templates` VALUES (1,9);
/*!40000 ALTER TABLE `caracteristicas_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `desgustaciones`
--

DROP TABLE IF EXISTS `desgustaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `desgustaciones` (
  `idDesgustacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `numDesgustador` int(11) NOT NULL,
  `numCopa` int(11) NOT NULL,
  `idCaracteristica` bigint(20) NOT NULL,
  `nota` float NOT NULL,
  `idTemplate` bigint(20) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idDesgustacion`),
  KEY `idCaracteristica` (`idCaracteristica`),
  KEY `idTemplate` (`idTemplate`),
  CONSTRAINT `desgustaciones_ibfk_2` FOREIGN KEY (`idTemplate`) REFERENCES `templates` (`idTemplate`),
  CONSTRAINT `desgustaciones_ibfk_3` FOREIGN KEY (`idCaracteristica`) REFERENCES `caracteristicas_templates` (`idCaracteristica`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `desgustaciones`
--

LOCK TABLES `desgustaciones` WRITE;
/*!40000 ALTER TABLE `desgustaciones` DISABLE KEYS */;
INSERT INTO `desgustaciones` VALUES (7,1,1,1,4,1,'2012-07-20 00:40:19');
INSERT INTO `desgustaciones` VALUES (8,2,1,1,5,1,'2012-07-20 00:41:00');
INSERT INTO `desgustaciones` VALUES (11,3,1,1,4,1,'2012-07-20 01:00:43');
INSERT INTO `desgustaciones` VALUES (12,4,1,1,3,1,'2012-07-20 01:00:43');
/*!40000 ALTER TABLE `desgustaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templates`
--

DROP TABLE IF EXISTS `templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templates` (
  `idTemplate` bigint(20) NOT NULL AUTO_INCREMENT,
  `cantidadCopas` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '0',
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaActivación` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idTemplate`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templates`
--

LOCK TABLES `templates` WRITE;
/*!40000 ALTER TABLE `templates` DISABLE KEYS */;
INSERT INTO `templates` VALUES (1,7,0,'2012-07-20 00:23:26','2012-07-20 00:23:26');
/*!40000 ALTER TABLE `templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiposexamenes`
--

DROP TABLE IF EXISTS `tiposexamenes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiposexamenes` (
  `idTipoExamen` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idTipoExamen`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiposexamenes`
--

LOCK TABLES `tiposexamenes` WRITE;
/*!40000 ALTER TABLE `tiposexamenes` DISABLE KEYS */;
INSERT INTO `tiposexamenes` VALUES (1,'Examen Visual');
INSERT INTO `tiposexamenes` VALUES (2,'Examen Olfativo');
INSERT INTO `tiposexamenes` VALUES (3,'Examen Gustativo');
/*!40000 ALTER TABLE `tiposexamenes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-08-06  4:20:27
