$(document).keypress(function(e){
    if (e.which == 13){
        logear();
    }
});
function logear(){
    $.getJSON("/cinclidosinta/ServletMaestro", { //llamada al servlet
        op: "1", //1 == LOGIN parámetros pasados
        username: $("#username").val(),
        password: $("#password").val() //devuelve el valor
    }, function(data) { //data es el nombre del parametro con el que se recibe la respuesta desde el servidor
        if(data.status == "ok"){
            window.location = "admin.jsp";
        }else if(data.status == "notOk"){
            if(!$("#username").hasClass("error")){
                $("#username").addClass("error");
                $("#password").addClass("error");
            }
            $("#error").html("Usuario o contraseña incorrecta");
        }else{
            alert("error");
        }
    });
}