function guardarExViasual(){
    $('#errorVisual').hide();
    $('#guardadoVisual').hide();
    $(document).ready(function(){
        
        var nombreEx = $('#nuevoVisual').val();
        if(nombreEx == ""){
            $("#errorVisual").show("slow")
        }else {
            $.getJSON("/cinclidosinta/ServletMaestro",{
                op:"11",
                nombre : nombreEx,
                tipoEx : 1
            })
            $('#guardadoVisual').show("slow");
            $('#nuevoVisual').val("");
        }
    });
}

function guardarExOlfativo(){
    $('#errorOlfativo').hide();
    $('#guardadoOlfativo').hide();
    $(document).ready(function(){
        
        var nombreEx = $('#nuevoOlfativo').val();
        if(nombreEx == ""){
            $("#errorOlfativo").show("slow")
        }else {
            $.getJSON("/cinclidosinta/ServletMaestro",{
                op:"11",
                nombre : nombreEx,
                tipoEx : 2
            })
            $('#guardadoOlfativo').show("slow");
            $('#nuevoOlfativo').val("");
        }
    })
}

function guardarExGustativo(){
    $('#errorGustativo').hide();
    $('#guardadoGustativo').hide();
    $(document).ready(function(){
        
        var nombreEx = $('#nuevoGustativo').val();
        if(nombreEx == ""){
            $("#errorGustativo").show("slow")
        }else {
            $.getJSON("/cinclidosinta/ServletMaestro",{
                op:"11",
                nombre : nombreEx,
                tipoEx : 3
            })
            $('#guardadoGustativo').show("slow");
            $('#nuevoGustativo').val("");
        }
    }) 
}
