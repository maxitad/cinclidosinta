<div class="modal hide fade" id="varietalModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">x</button>
        <h3>Tratamiento</h3>
    </div>
    <div class="modal-body" id="formVarietal">
        <h5>NOMBRE DEL ENSAYO</h5>
        <input class="input-block-level focused" type="text" data-provide="typeahead" id="nombreVarietal">
        <h5>PROCEDENCIA</h5>
        <input class="input-block-level focused" type="text" data-provide="typeahead" id="nombreBodega">
        <h5>A�O DE COSECHA</h5>
        <input class="input-block-level focused" type="text" data-provide="typeahead" id="anioCosecha">        
    </div>
    <div class="modal-footer">
        <div class="btn-group">
            <button class="btn btn-primary" data-dismiss="modal" onclick="nuevoVarietal()" tabindex="-1" style="width:50%;" type="button">Agregar</button>
            <button class="btn" data-dismiss="modal" tabindex="-1" onclick="limpiarVarietalNuevo()" type="button" style="width:50%;">Cancelar</button>
        </div>
    </div>
</div>