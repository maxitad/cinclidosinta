<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
       <meta name="tipo_contenido"  content="text/html;" http-equiv="content-type" charset="utf-8">
        <title>Sesion</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Tesla Software Factory S.A.">

        <link href="../../assets/css/bootstrap.css" rel="stylesheet">
        <link href="../../assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!--<link rel="shortcut icon" href="../assets/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">-->
        <script type="text/javascript">
            window.onload = function(){
                gustativoFc();
                visualFc();
                olfativoFc();
                tratamientoFc();
                
            }
        </script>
    </head>

    <body>
        <%@include file="header.jsp" %>
        <div class="container" style="margin-top: 60px">
            <div class="row">
                <div class="span8 offset2 well">
                    <div class="page-header">
                        <h2>Nombrar Sesión</h2>
                         <input class="input-block-level focused" type="text" data-provide="typeahead" id="nombreSesion"> 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="span8 offset2 well">
                    <div class="page-header">
                        <h2>Agregar Tratamientos</h2>
                        <a data-toggle="modal" href="#varietalModal"><i class="icon-glass icon-black"></i> Tratamiento Nuevo</a>
                    </div>
                    <div class="row">
                        <div class="span4" id="tratamientos">
                        </div>
                    </div>
                </div>
                <div id="opciones">

                </div>
            </div>
            <div class="row">
                <div class="span8 offset2 well">
                    <div class="page-header">
                        <h2>Agregar Características</h2>
                    </div>
                    <a data-toggle="modal" href="#caracteristicaModal"><i class="icon-tag icon-black"></i> Característica Nueva</a>
                    <div class="row-fluid" id="tipoExamenes">
                        <div class="row-fluid">
                            <div class="span4 btn-group btn-group-vertical">
                                <h3>Examen Visual</h3>
                                <!-- <label for="name" id="name_label">Name</label>  
                                 <input type="text" name="name" id="nuevoVisual" value="" class="text-input" />
                                 <div hidden="true" id="errorVisual"><span class="label label-important" >Error!</span></div>
                                 <div hidden="true" id="guardadoVisual"><span class="label label-success">Guardado!</span></div> 
                                 <button type="submit" onclick="guardarExViasual()" id="agregarVisual" name="submit" class="btn btn-primary">Agregar</button> -->
                                <br>
                                <div id="exvisual"></div>

                            </div>
                            <div class="span4 btn-group btn-group-vertical">
                                <h3>Examen Olfativo</h3>

                                <br>
                                <div id="exolfativo"></div>
                            </div>

                            <div class="span4 btn-group btn-group-vertical">
                                <h3>Examen Gustativo</h3>

                                <br>
                                <div id="exgustativo"></div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
        <div class="row">
            <div class="span6 offset4 well">
                <div class="btn-group">
                    <button class="btn btn-primary btn-large" onclick="tratamientoSeleccionado()" tabindex="-1" style="width:50%;" type="button">Guardar Cambios</button>
                    <button class="btn btn-large" onclick="regresar()" tabindex="-1" style="width:50%;" type="button">Cancelar</button>
                </div>
            </div>  
        </div>
    <%@include file="nuevaCaracteristica.jsp" %>
    <%@include file="nuevoVarietal.jsp" %>


    <script src="../../assets/js-api/jquery-1.7.2.min.js"></script>
    <script src="../../assets/js-api/bootstrap.js"></script>
    <script src="../../assets/js/template.js"></script>
    <script src="../../assets/js/nuevaCaracteristica.js"></script>
    <script src="../../assets/js/admin.js"></script>
    <script>
        $('.typeahead').typeahead();
        // $('#caracteristicaModal').modal(option);
    </script>

</body>
</html>