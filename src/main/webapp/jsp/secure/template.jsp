<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Sesion</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Tesla Software Factory S.A.">

        <link href="../../assets/css/bootstrap.css" rel="stylesheet">
        <link href="../../assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!--<link rel="shortcut icon" href="../assets/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">-->
    </head>

    <body>
        <%@include file="header.jsp" %>
        <div class="container" style="margin-top: 60px">
            <div class="row">
                <div class="span8 offset2 well">
                    <div class="page-header">
                        <h2>Agregar Varietales</h2>
                    </div>
                    <div class="row">
                        <div class="span4">
                            <select class="input-block-level" multiple="multiple" style="height: 193px">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                        <div class="span3">
                            <h5>NOMBRE DEL VARIETAL</h5>
                            <input class="input-block-level focused" type="text" data-provide="typeahead">
                            <h5>NOMBRE DE LA BODEGA</h5>
                            <input class="input-block-level focused" type="text" data-provide="typeahead">
                            <h5>AÑO DE COSECHA</h5>
                            <input class="input-block-level focused" type="text" data-provide="typeahead">
                            <div class="btn-group">
                                <button class="btn btn-primary" onclick="logear()" tabindex="-1" style="width:50%;" type="button">Agregar</button>
                                <button class="btn" onclick="volver()" tabindex="-1" type="button" style="width:50%;">Quitar</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="span8 offset2 well">
                    <div class="page-header">
                        <h2>Agregar Características</h2>
                    </div>
                    <div class="row">
                        <div class="tabbable"> 
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab1" data-toggle="tab">Exámen Visual</a></li>
                                <li><a href="#tab2" data-toggle="tab">Exámen Olfativo</a></li>
                                <li><a href="#tab3" data-toggle="tab">Exámen Gustativo</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab1">
                                    <div class="span4" id="exvisual">
                                        <select id="exvisual" class="input-block-level" multiple="multiple" 
                                               style="height: 138px;">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option> 
                                        </select>
                                    </div>
                                    <div class="span3">
                                        <h5>CARACTERÍSTICA</h5>
                                        <input class="input-block-level focused" type="text" data-provide="typeahead">
                                        <div class="btn-group">
                                            <button class="btn btn-primary" onclick="seleccionCaracter()" tabindex="-1" style="width:50%;" type="button">Agregar</button>
                                            <button class="btn" onclick="volver()" tabindex="-1" type="button" style="width:50%;">Quitar</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab2">
                                    <div class="tab-pane" id="tab2">
                                        <div class="span4">
                                            <select class="input-block-level" multiple="multiple" style="height: 138px;">
                                                <option>a</option>
                                                <option>b</option>
                                                <option>c</option>
                                                <option>d</option>
                                                <option>e</option>
                                            </select>
                                        </div>
                                        <div class="span3">
                                            <h5>CARACTERÍSTICA</h5>
                                            <input class="input-block-level focused" type="text" data-provide="typeahead">
                                            <div class="btn-group">
                                                <button class="btn btn-primary" onclick="logear()" tabindex="-1" style="width:50%;" type="button">Agregar</button>
                                                <button class="btn" onclick="volver()" tabindex="-1" type="button" style="width:50%;">Quitar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab3">
                                    <div class="tab-pane" id="tab3">
                                        <div class="span4" id="exgustativo">
                                            
                                        </div>
                                        <div class="span3">
                                            <h5>CARACTERÍSTICA</h5>
                                            <input class="input-block-level focused" type="text" data-provide="typeahead">
                                            <div class="btn-group">
                                                <button class="btn btn-primary" onclick="logear()" tabindex="-1" style="width:50%;" type="button">Agregar</button>
                                                <button class="btn" onclick="volver()" tabindex="-1" type="button" style="width:50%;">Quitar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div>
                <select id="opciones" name="garden" multiple="multiple">
                    <option>Flowers</option>
                    <option selected="selected">Shrubs</option>
                    <option>Trees</option>
                    <option selected="selected">Bushes</option>
                    <option>Grass</option>
                    <option>Dirt</option>
                </select>
                <div id="ruinas">Flowers Shrubs Trees Bushes Grass Dirt </div>
                <button class="btn btn-primary" onclick="gustativoFc()" tabindex="-1" style="width:50%;" type="button">Consultar</button>
                <script>window.onload = (function(){
                    try{

                        $('#opciones').change(function () {
                            var str = "";
                            $("select option:selected").each(function () {
                                str += $(this).text() + " ";
                            });
                            $('#ruinas').text(str);
                        })
                        .trigger('change');

                    }catch(e){}});</script>
            </div>

        </div>
        <div class="row">
            <div class="span6 offset4 well">
                <div class="btn-group">
                    <button class="btn btn-primary btn-large" onclick="s()" tabindex="-1" style="width:50%;" type="button">Guardar Cambios</button>
                    <button class="btn btn-large" onclick="s()" tabindex="-1" style="width:50%;" type="button">Cancelar</button>
                </div>
            </div>  
        </div>
    </div>



    <script src="../../assets/js-api/jquery-1.7.2.min.js"></script>
    <script src="../../assets/js-api/bootstrap.js"></script>
    <script src="../../assets/js/template.js"></script>
    <script>
    $('.typeahead').typeahead()
    </script>

</body>
</html>