<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Tesla Software Factory S.A.">

        <link href="../../assets/css/bootstrap.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }
        </style>
        <link href="../../assets/css/bootstrap-responsive.css" rel="stylesheet">
        <link href="../../assets/css/estilos.css" rel="stylesheet">

        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!--<link rel="shortcut icon" href="../assets/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">-->
    </head>

    <body>
        <div class="container loginadmin">
            <div class="row">
                <div class="span4 offset4 well">
                    <h1 style="text-align: center;">Administrar</h1><br>
                    <form class="form-vertical">
                        
                        <h5>Usuario</h5>
                        <input id="username" class="input-block-level focused" autofocus type="text">
                        
                        <label>Contraseña</label>
                        <input id="password" class="input-block-level focused" type="password">
                        <br><br>
                        <div class="btn-group">
                            <button class="btn btn-primary btn-large" onclick="logear()" style="width:50%;" tabindex="-1" type="button">Entrar</button>
                            <button class="btn btn-large" onclick="location.href='../../index.jsp'" style="width:50%;' tabindex="-1" type="button">Volver</button>
                        </div>
                        <span class="control-group error">
                            <span id="error" class="help-inline"></span>
                        </span>
                    </form>
                </div>
            </div>

            <footer>
                <p>&copy; Tesla Software Factory 2012</p>
            </footer>

        </div>

        <script src="../../assets/js-api/jquery-1.7.2.min.js"></script>
        <script src="../../assets/js-api/bootstrap.js"></script>
        <script src="../../assets/js/loginadmin.js"></script>
    </body>
</html>