<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Bienvenido!</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Tesla Software Factory S.A.">

        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }
        </style>
        <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
        <link href="assets/css/estilos.css" rel="stylesheet">

        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!--<link rel="shortcut icon" href="../assets/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">-->
     <script type="text/javascript">
            window.onload = function(){
                carga();
            }
           </script>
    </head>

    <body>
        <div class="container index">
            <div class="row">
                <div class="span4 offset4 well">
                    <div class="page-header">
                        <h1 class="titulo">Sesión </h1><br>
                        <h6>Por favor, ingrese sus datos para comenzar la catación</h6>
                    </div>
                    <form class="form-vertical">
                        <h5>Nombre</h5>
                        <input id="nombre" class="input-block-level focused" autofocus type="text">
                        <h5>Apellido</h5>
                        <input id="apellido" class="input-block-level focused" type="text">
                        <h5>Procedencia</h5>
                        <input id="procedencia" class="input-block-level focused" type="text">
                        <br><br>
                        <div class="btn-group">
                            <button id="aceptar" class="btn btn-primary btn-large" onclick="aceptado()" style="width:50%;" tabindex="-1" type="button">Aceptar</button>
                            <button id="limpiar" class="btn btn-large disabled" onclick="limpiar()" style="width:50%;" tabindex="-1" type="button">Limpiar</button>
                        </div>
                        <span class="control-group error">
                            <span id="error" class="help-inline"></span>
                        </span>
                    </form>
                </div>
            </div>
            <div class="row"> 
                <form class="form-vertical">
                    <button class="btn btn-large" onClick="location.href='jsp/secure/loginAdmin.jsp'" tabindex="-1" type="button">Administrador</button>
                </form>
            </div>
        </div>

        <script src="assets/js-api/jquery-1.7.2.min.js"></script>
        <script src="assets/js-api/bootstrap.js"></script>
        <script src="assets/js/login.js"></script>

    </body>
</html>