-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 17-10-2012 a las 16:24:21
-- Versión del servidor: 5.5.24
-- Versión de PHP: 5.3.10-1ubuntu3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cinclidos_inta`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administradores`
--

CREATE TABLE IF NOT EXISTS `administradores` (
  `idAdministrador` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `usuario` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `clave` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idAdministrador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `degustadores`
--

CREATE TABLE IF NOT EXISTS `degustadores` (
  `idDegustador` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `procedencia` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idDegustador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examenes`
--

CREATE TABLE IF NOT EXISTS `examenes` (
  `idExamen` bigint(20) NOT NULL AUTO_INCREMENT,
  `idFTipoExamen` bigint(20) NOT NULL,
  `nombre` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idExamen`),
  KEY `idTipoExamen` (`idFTipoExamen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `templates`
--

CREATE TABLE IF NOT EXISTS `templates` (
  `idTemplate` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idTemplate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `template_examenes`
--

CREATE TABLE IF NOT EXISTS `template_examenes` (
  `idB` bigint(20) NOT NULL AUTO_INCREMENT,
  `idFTemplate` bigint(20) NOT NULL,
  `idFExamen` bigint(20) NOT NULL,
  PRIMARY KEY (`idB`),
  KEY `idFTemplate` (`idFTemplate`,`idFExamen`),
  KEY `idFExamen` (`idFExamen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `template_notas`
--

CREATE TABLE IF NOT EXISTS `template_notas` (
  `idA` bigint(20) NOT NULL,
  `idB` bigint(20) NOT NULL,
  `nota` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `idFDegustador` bigint(20) NOT NULL,
  PRIMARY KEY (`idA`,`idB`),
  KEY `idDegustador` (`idFDegustador`),
  KEY `idB` (`idB`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `template_tratamientos`
--

CREATE TABLE IF NOT EXISTS `template_tratamientos` (
  `idA` bigint(20) NOT NULL AUTO_INCREMENT,
  `idFTemplate` bigint(20) NOT NULL,
  `idFTratamiento` bigint(20) NOT NULL,
  PRIMARY KEY (`idA`),
  KEY `idFTemplate` (`idFTemplate`,`idFTratamiento`),
  KEY `idFTratamiento` (`idFTratamiento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiposexamenes`
--

CREATE TABLE IF NOT EXISTS `tiposexamenes` (
  `idTipoExamen` bigint(20) NOT NULL AUTO_INCREMENT,
  `examen` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idTipoExamen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tratamientos`
--

CREATE TABLE IF NOT EXISTS `tratamientos` (
  `idTratamiento` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombreEnsayo` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `nombreOrigen` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `anioProduccion` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idTratamiento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `examenes`
--
ALTER TABLE `examenes`
  ADD CONSTRAINT `examenes_ibfk_1` FOREIGN KEY (`idFTipoExamen`) REFERENCES `tiposexamenes` (`idTipoExamen`);

--
-- Filtros para la tabla `template_examenes`
--
ALTER TABLE `template_examenes`
  ADD CONSTRAINT `template_examenes_ibfk_1` FOREIGN KEY (`idFTemplate`) REFERENCES `templates` (`idTemplate`),
  ADD CONSTRAINT `template_examenes_ibfk_2` FOREIGN KEY (`idFExamen`) REFERENCES `examenes` (`idExamen`);

--
-- Filtros para la tabla `template_notas`
--
ALTER TABLE `template_notas`
  ADD CONSTRAINT `template_notas_ibfk_1` FOREIGN KEY (`idA`) REFERENCES `template_tratamientos` (`idA`),
  ADD CONSTRAINT `template_notas_ibfk_2` FOREIGN KEY (`idB`) REFERENCES `template_examenes` (`idB`),
  ADD CONSTRAINT `template_notas_ibfk_3` FOREIGN KEY (`idFDegustador`) REFERENCES `degustadores` (`idDegustador`);

--
-- Filtros para la tabla `template_tratamientos`
--
ALTER TABLE `template_tratamientos`
  ADD CONSTRAINT `template_tratamientos_ibfk_1` FOREIGN KEY (`idFTemplate`) REFERENCES `templates` (`idTemplate`),
  ADD CONSTRAINT `template_tratamientos_ibfk_2` FOREIGN KEY (`idFTratamiento`) REFERENCES `tratamientos` (`idTratamiento`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
