/**
 *
 */
function guardarCaracteristica(){
    $(document).ready(function(){
        var str1 = "";
        var str2 = "";
        var str3 = "";
        //$("active tabe-pane:active");
        str1 = $("#nuevoVisual").text();
        str2 = $("#nuevoVisual").text();
        str3 = $("#nuevoGustativo").text();
        alert(str1 + str2 + str3);
       
    });    
}

function cargarCarac(){
    $.getJSON("/cinclidosinta/ServletMaestro", {
        op:"3"
    }, function(data){
            
        }
        );
}

function seleccionCaracter(){
    $(document).ready(function(){
        $('select').change(function(){
            var str = "" ;
            $("select option:selected").each(function(){
                str += $(this).text + "" ;
            })
            .trigger('change'); 
        }); 
    });
}



function exVisual(){
    try{
        $(document).ready(function(){
            $('#exvisual').change(function(){
                var str = "" ;
                $("select option:selected").each(function(){
                    str += $(this).text + " " ;
                });
                $('#formaSesion').text(str);
            }) 
            .trigger('change'); 
        });
    }catch(e){}
}

function cargaModos(){
    $(document).ready(function(){
        $.getJSON("/cinclidosinta/ServletMaestro",{
            op:"3"
        }, 
        function(data){
            $("listaA").addItems(data);
        });
    });
    
    $.fn.addItems = function(data){
        return this.each(function(){
            var list = this;
            $.each(data, function(index, itemData){
                var option = new Select(itemData.Value);
                list.add(option);
            });
        });
    };
}

/**
 * función que trae un json desde el servlet con los examenes de tipo gustativo
 */
function gustativoFc(){
    $(document).ready(function(){ 
        $.getJSON("/cinclidosinta/ServletMaestro",{
            op:"3"
        }, 
        function(data){
        {  
            $.each(data, function(i, product) {
                var option = $('<button id="'+product.idCaracteristica+'" class="btn span12" data-toggle="button" style="clear: both !important;">').text(product.nombre).appendTo($('#exgustativo'));
                $('<option>').text(product.nombre).appendTo(option);
            });
        }  
        });  
    });  
}

/**
 * función que trae un json desde el servlet con los examenes de tipo Visual.
 */
function visualFc(){
    $(document).ready(function(){ 
        $.getJSON("/cinclidosinta/ServletMaestro",{
            op:"4"
        }, 
        function(data){
        {  
            $.each(data, function(i, product) {
                var option = $('<button id="'+product.idCaracteristica+'" class="btn span12" data-toggle="button" style="clear: both !important;">').text(product.nombre).appendTo($('#exvisual'));
            });
        }  
        });  
    });  
}

/**
 * función que trae un json desde el servlet con los examenes de tipo Olfativo.
 */
function olfativoFc(){
    $(document).ready(function(){ 
        $.getJSON("/cinclidosinta/ServletMaestro",{
            op:"5"
        }, 
        function(data){
        {  
            $.each(data, function(i, product) {
                var option = $('<button id="'+product.idCaracteristica+'" class="btn span12" data-toggle="button" style="clear: both !important;">').text(product.nombre).appendTo($('#exolfativo'));
                $('<option>').text(product.nombre).appendTo(option);
            });
        }  
        });  
    });  
}


/**
 * función que trae un json desde el servlet con los tratmientos (vinos).
 */
function tratamientoFc(){
    $(document).ready(function(){ 
        $.getJSON("/cinclidosinta/ServletMaestro",{
            op:"6"
        }, 
        function(data){
        {  
            var ide = new Array();
            var origen = new Array();
            var añoCosecha = new Array();
            var nombrel = new Array();
            $.each(data, function(i, product) {
                
                i++;
                l=i;
                ide[i] = product.id;
                origen[i] = product.origen;
                nombrel[i] = product.nombre;
                añoCosecha[i] = product.añoCosecha;
                var option = $('<button id="'+product.id+'" class="btn span3" data-toggle="button" style="clear: both !important;">').text(product.nombre).appendTo($('#tratamientos'));
            });
        }
        });  
        
    });  
}

function tratamientoSeleccionado(){
   // validar();
    var nombreSesion = $("#nombreSesion").val();
    var arrayDetipoExamenesSeleccionados = "";
    $.each($("#tipoExamenes .active"),function(i, data){
        arrayDetipoExamenesSeleccionados += $(this).attr("id") + " ";
    });
    
    var arrayDeTratamientosSeleccionados = "";
    $.each($("#tratamientos .active"),function(i, data){
        arrayDeTratamientosSeleccionados += $(this).attr("id") + " ";
    });
    
    $.getJSON("/cinclidosinta/ServletMaestro",{
        op:"7", 
        tratamientos : arrayDeTratamientosSeleccionados,
        examenes : arrayDetipoExamenesSeleccionados,
        nombre : nombreSesion
    }, 
    function(data){
      window.location="admin.jsp";
        });
   
}

function examenesSeleccionados(){
    var examenes = $('#tipoExamenes .active');
    var sesionExamenes, i = 0;
    $.each(examenes, function(i,val){
        sesionExamenes.examenes[i++] = $(this).attr("id");
    });
    
    $.each(examenes, function(i,val){
        
        });
}

function validar(){
    $("#nombreSesion").validate();
    
}

function regresar(){
    window.location="admin.jsp";
}