<div class="modal hide fade" id="caracteristicaModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">x</button>
        <h3>Agregar Caracter�stica</h3>
    </div>
    <div class="modal-body">
        <div class="tabbable">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab1" data-toggle="tab">Ex�men Visual</a></li>
                <li><a href="#tab2" data-toggle="tab">Ex�men Olfativo</a></li>
                <li><a href="#tab3" data-toggle="tab">Ex�men Gustativo</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab1">
                    <h5>CARACTER�STICA</h5>
                    <input type="text" name="name" id="nuevoVisual" value="" class="text-input" />
                    <div hidden="true" id="errorVisual"><span class="label label-important" >Error!</span></div>
                    <div hidden="true" id="guardadoVisual"><span class="label label-success">Guardado!</span></div>
                    <button type="submit" onclick="guardarExViasual()" id="agregarVisual" name="submit" class="btn btn-primary">Agregar</button>
                </div>
                <div class="tab-pane" id="tab2">
                    <h5>CARACTER�STICA</h5>
                    <input type="text" name="name" id="nuevoOlfativo" value="" class="text-input" />
                    <div hidden="true" id="errorOlfativo"><span class="label label-important" >Error!</span></div>
                    <div hidden="true" id="guardadoOlfativo"><span class="label label-success">Guardado!</span></div>
                    <input type="submit" onclick="guardarExOlfativo()" id="agregarOlfativo" name="submit" class="btn btn-primary" value="Agregar">
                </div>
                <div class="tab-pane" id="tab3">
                    <h5>CARACTER�STICA</h5>
                    <input type="text" name="name" id="nuevoGustativo" value="" class="text-input" /> 
                    <div hidden="true" id="errorGustativo"><span class="label label-important" >Error!</span></div>
                    <div hidden="true" id="guardadoGustativo"><span class="label label-success">Guardado!</span></div>
                    <input type="submit" onclick="guardarExGustativo()" id="agregarGustativo" name="submit" class="btn btn-primary" value="Agregar">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="btn-group">
                <!--<button class="btn btn-primary" onclick="guardarCaracteristica()" tabindex="-1" style="width:50%;" type="button">Agregar</button> -->
                <button class="btn btn-primary" data-dismiss="modal" tabindex="-1" type="button" style="width:50%;">Listo!</button> 
            </div>
        </div>
    </div>
</div>
