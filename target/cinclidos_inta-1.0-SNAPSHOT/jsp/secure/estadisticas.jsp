<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Admin</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Tesla Software Factory S.A.">

        <link href="../../assets/css/bootstrap.css" rel="stylesheet">
        <link href="../../assets/css/bootstrap-responsive.css" rel="stylesheet">
        <style>
            /*.subnav {
                background-image: -moz-linear-gradient(center top , #F5F5F5 0%, #EEEEEE 100%);
            }*/
            .subnav {
                width: 100%;
                height: 36px;
                background-color: #eeeeee; /* Old browsers */
                background-repeat: repeat-x; /* Repeat the gradient */
                background-image: -moz-linear-gradient(top, #f5f5f5 0%, #eeeeee 100%); /* FF3.6+ */
                background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f5f5f5), color-stop(100%,#eeeeee)); /* Chrome,Safari4+ */
                background-image: -webkit-linear-gradient(top, #f5f5f5 0%,#eeeeee 100%); /* Chrome 10+,Safari 5.1+ */
                background-image: -ms-linear-gradient(top, #f5f5f5 0%,#eeeeee 100%); /* IE10+ */
                background-image: -o-linear-gradient(top, #f5f5f5 0%,#eeeeee 100%); /* Opera 11.10+ */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f5f5f5', endColorstr='#eeeeee',GradientType=0 ); /* IE6-9 */
                background-image: linear-gradient(top, #f5f5f5 0%,#eeeeee 100%); /* W3C */
                border: 1px solid #e5e5e5;
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                border-radius: 4px;
                margin-top: 60px;
            }
            .subnav-fixed{
                margin: 0 !important;
            }
            .subnav .nav {
                margin-bottom: 0;
            }
            .subnav .nav > li > a {
                margin: 0;
                padding-top:    11px;
                padding-bottom: 11px;
                border-left: 1px solid #f5f5f5;
                border-right: 1px solid #e5e5e5;
                -webkit-border-radius: 0;
                -moz-border-radius: 0;
                border-radius: 0;
            }
            .subnav .nav > .active > a,
            .subnav .nav > .active > a:hover {
                padding-left: 13px;
                color: #777;
                background-color: #e9e9e9;
                border-right-color: #ddd;
                border-left: 0;
                -webkit-box-shadow: inset 0 3px 5px rgba(0,0,0,.05);
                -moz-box-shadow: inset 0 3px 5px rgba(0,0,0,.05);
                box-shadow: inset 0 3px 5px rgba(0,0,0,.05);
            }
            .subnav .nav > .active > a .caret,
            .subnav .nav > .active > a:hover .caret {
                border-top-color: #777;
            }
            .subnav .nav > li:first-child > a,
            .subnav .nav > li:first-child > a:hover {
                border-left: 0;
                padding-left: 12px;
                -webkit-border-radius: 4px 0 0 4px;
                -moz-border-radius: 4px 0 0 4px;
                border-radius: 4px 0 0 4px;
            }
            .subnav .nav > li:last-child > a {
                border-right: 0;
            }
            .subnav .dropdown-menu {
                -webkit-border-radius: 0 0 4px 4px;
                -moz-border-radius: 0 0 4px 4px;
                border-radius: 0 0 4px 4px;
            }
            @media (max-width: 979px) and (min-width: 768px) {
                .subnav {
                    margin: 0 !important;
                }
            }
            @media (max-width: 768px) {
                .subnav {
                    position: static;
                    top: auto;
                    z-index: auto;
                    width: auto;
                    height: auto;
                    background: #fff; /* whole background property since we use a background-image for gradient */
                    -webkit-box-shadow: none;
                    -moz-box-shadow: none;
                    box-shadow: none;
                    margin: 0 !important;
                }
                .subnav .nav > li {
                    float: none;
                }
                .subnav .nav > li > a {
                    border: 0;
                }
                .subnav .nav > li + li > a {
                    border-top: 1px solid #e5e5e5;
                }
                .subnav .nav > li:first-child > a,
                .subnav .nav > li:first-child > a:hover {
                    -webkit-border-radius: 4px 4px 0 0;
                    -moz-border-radius: 4px 4px 0 0;
                    border-radius: 4px 4px 0 0;
                }
            }

            @media (min-width: 980px) {
                .subnav-fixed {
                    position: fixed;
                    top: 40px;
                    left: 0;
                    right: 0;
                    z-index: 1020; /* 10 less than .navbar-fixed to prevent any overlap */
                    border-color: #d5d5d5;
                    border-width: 0 0 1px; /* drop the border on the fixed edges */
                    -webkit-border-radius: 0;
                    -moz-border-radius: 0;
                    border-radius: 0;
                    -webkit-box-shadow: inset 0 1px 0 #fff, 0 1px 5px rgba(0,0,0,.1);
                    -moz-box-shadow: inset 0 1px 0 #fff, 0 1px 5px rgba(0,0,0,.1);
                    box-shadow: inset 0 1px 0 #fff, 0 1px 5px rgba(0,0,0,.1);
                    filter: progid:DXImageTransform.Microsoft.gradient(enabled=false); /* IE6-9 */
                }
                .subnav-fixed .nav {
                    width: 938px;
                    margin: 0 auto;
                    padding: 0 1px;
                }
                .subnav .nav > li:first-child > a,
                .subnav .nav > li:first-child > a:hover {
                    -webkit-border-radius: 0;
                    -moz-border-radius: 0;
                    border-radius: 0;
                }
            }

            @media (min-width: 1210px) {
                .subnav{
                    margin-top: 60px;
                }
                .subnav-fixed .nav {
                    width: 1168px; /* 2px less to account for left/right borders being removed when in fixed mode */
                }

            }​
        </style>

        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!--<link rel="shortcut icon" href="../assets/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">-->
    </head>

    <body>
        <%@include file="header.jsp" %>
        <div class="container">
            <div class="subnav">
                <ul class="nav nav-pills pull-left">
                    <li class="dropdown"><a href="#" class="dropdown-togle" data-toggle="dropdown"><i class="icon-pencil icon-black"></i> Nuevo <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="nuevaSesion()"><i class="icon-list icon-black"></i> Sesión Nueva</a>
                            <li><a data-toggle="modal" href="#varietalModal"><i class="icon-glass icon-black"></i> Varietal Nuevo</a>
                            <li><a data-toggle="modal" href="#caracteristicaModal"><i class="icon-tag icon-black"></i> Característica Nueva</a>
                        </ul>
                    <li><a href="#" onclick="copiar()"><i class="icon-briefcase icon-black"></i> Copiar</a>
                    <li><a id="eliminar" href="#" onclick="eliminar()"><i class="icon-trash icon-black"></i> Eliminar</a>
                    <li><a href="estadisticas.jsp" onclick="estadisticas()"><i class="icon-tasks icon-black"></i> Estadísticas</a>
                    <li><a id="activacion" href="#" onclick="activacion()"><i class="icon-off icon-black"></i> Activar</a>
                    <li><div class="navbar-form">
                            <div class="input-append"><input type="text" class="input-medium search-query"><button class="btn" type="button"><i class="icon-search icon-black"></i></button></div>
                        </div></li>
                </ul>               
                <ul class="nav nav-pills pull-right">
                    <li><a href="#"><<</a></li>
                    <li class="active">
                        <a href="#">1</a>
                    </li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">>></a></li>
                </ul>

            </div>
        </div>
        
        <!-- HTML de Modales para agregar items a la BD-->
        <%@include file="nuevoVarietal.jsp"%>
        <%@include file="nuevaCaracteristica.jsp" %>
        <!-- Termina el HTML-->
        
        <script src="../../assets/js-api/jquery-1.7.2.min.js"></script>
        <script src="../../assets/js-api/bootstrap.js"></script>
        <script src="../../assets/js/admin.js"></script>
        <script>
            $('#varietalModal').modal(option)
            $('#caracteristicaModal').modal(option)
        </script>
    </body>
</html>