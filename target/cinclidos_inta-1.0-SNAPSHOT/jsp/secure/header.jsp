<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="admin.jsp">Herramientas Administrativas</a>
            <div class="nav-collapse">
                <ul class="nav">
                    <li><a href="#" onclick="logoff()">Salir</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</div>